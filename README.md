# todo-challenge-backend
This repository contains the backend implementation of the Todo Challenge application.
## Prerequisites
1. Java Development Kit (JDK) 11 or later
2. Maven
3. MySQL database server

## Configuration
#### MySQL Database
1. ##### Database Setup:
   * Create a MySQL database for the application. You can do this using a tool like MySQL Workbench or by running SQL commands in a MySQL command line interface:
    ```
    CREATE DATABASE todo_challenge_db;
    ```
   * 
2. ##### Application Configuration:
   * Open the src/main/resources/application.properties file.
   * Update the following properties with your MySQL database credentials:

    ```
    spring.datasource.url=jdbc:mysql://localhost:3306/todo_challenge_db?useSSL=false
    spring.datasource.username=<your_mysql_username>
    spring.datasource.password=<your_mysql_password>
    ```

## Running the Application
To run the application locally, follow these steps:

1. ### Build the Project:
*    Open a terminal and navigate to the root directory of the project.
*    Run the following command to build the project:
     ```
     mvn clean install
     ```
2. ### Run the Application:
*   After the build is successful, run the following command to start the application:

    ```
    java -jar target/todo-challenge.jar
    ```
* Tables that we define in models package will be automatically generated in Database.
* You also need to add some rows into roles table before assigning any role to User.
* Run following SQL insert statements:

    ```
    INSERT INTO roles(name) VALUES('ROLE_USER');
    ```

3. ### Access the API:
* Once the application is running, you can access the API endpoints at http://localhost:8090/api.
* Register some users with signup API (or signup page with the frontend)


# Deploying the Application (Docker)
To deploy the application to a server, follow these steps:

1. ## Containerize the Application:
    * Build a Docker image for the application by running the following command:
   
    ```
    mvn clean package
    ```
2. ## Run the Docker Container:

   * After the Maven build is successful, navigate to the project directory containing the Dockerfile.

   * Run the following command to build a Docker image:
   
    ```
    docker build -t todo-challenge .
    ```
   * Once the Docker image is built, run the following command to start a Docker container:
   
    ```
    docker run -d -p 8090:8090 todo-challenge
    ```
3. ## Access the API:
   Once the Docker container is running, you can access the API endpoints at `http://<your_server_ip>:8090/api`