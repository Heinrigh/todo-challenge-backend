package com.henry.todochallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoChallengeApplication.class, args);
	}

}
